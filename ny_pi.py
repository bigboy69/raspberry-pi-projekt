#Scrolling display

#from sense_hat import SenseHat
#sense = SenseHat()
#
#blue = (0,0,255)
#yellow = (255,255,0)
#
#while True:
#    sense.show_message("BIF",text_colour=yellow, back_colour=blue, scroll_speed=0.1)

#Display letter

#from sense_hat import SenseHat
#from time import sleep

#sense = SenseHat()

#blue = (0,0,255)
#yellow = (255,255,0)

#while True:
#    sense.show_letter("B", yellow, blue)
#    sleep(1)
#    sense.show_letter("I", blue, yellow)
#    sleep(1)
#    sense.show_letter("F", yellow, blue)
#    sleep(1)

#matrix display

#from sense_hat import SenseHat
#
#sense = SenseHat()
#
#g = (255, 255, 0)
#b = (0, 0, 0) 
#
#matrix = [
#    g, g, g, g, g, g, g, g,
#    g, b, b, g, g, b, b, g,
#    g, b, b, g, g, b, b, g,
#    g, g, g, g, g, g, g, g,
#    g, g, g, g, g, g, g, g,
#    g, b, g, g, g, g, b, g,
#    g, g, b, b, b, b, g, g,
#    g, g, g, g, g, g, g, g
#]
#
#sense.set_pixels(matrix)

#temperatur
#
#from sense_hat import SenseHat
#
#sense = SenseHat()
#sense.clear()
#
#temp = sense.get_temperature()
#print(temp)

#joystick

from sense_hat import SenseHat
from time import sleep
sense = SenseHat()

yw = (255, 255, 0)
y = 4
x = 4

ball = sense.set_pixel(x, y,yw)

while True:
  for event in sense.stick.get_events():
    
    if event.action == "pressed":
      
      if event.direction == "up":
        sense.clear()
        y -= 1
        sense.set_pixel(x,y,yw)      
      if event.direction == "down":
        sense.clear()
        y += 1
        sense.set_pixel(x,y,yw)     
      if event.direction == "left": 
        sense.clear()
        x -= 1
        sense.set_pixel(x,y,yw)    
      if event.direction == "right":
        sense.clear()
        x += 1
        sense.set_pixel(x,y,yw)     
      if event.direction == "middle":
        sense.clear()
        sense.set_pixel(x,y,yw)     
    
      sleep(0.01)
    